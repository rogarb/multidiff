#!/usr/bin/env python3

import argparse
import filecmp

parser = argparse.ArgumentParser(
    prog="multidiff",
    description="A program to compare file by file a list of files",
)

parser.add_argument("--version", "-V", action="version", version="0.2.0")
parser.add_argument("files", help="The list of files to compare", nargs="+")
parser.add_argument(
    "--quiet",
    "-q",
    help="Output only the names of the files which differ",
    action="store_true",
    required=False,
)

args = parser.parse_args()

for i in range(0, len(args.files) - 1):
    for j in range(i + 1, len(args.files)):
        # print("Comparing files: {} and {}".format(args.files[i], args.files[j]))
        # if not filecmp.cmp(file1, file2, shallow=False):
        if args.quiet:
            if not filecmp.cmp(args.files[i], args.files[j], shallow=False):
                print("{} and {} differ".format(args.files[i], args.files[j]))
        else:
            different = False
            with open(args.files[i], "rb") as file1:
                with open(args.files[j], "rb") as file2:
                    curpos = 0
                    while True:
                        bufsize = 4096
                        # size of the buffer
                        bytes1 = file1.read(bufsize)
                        bytes2 = file2.read(bufsize)
                        minread = min(len(bytes1), len(bytes2))
                        for b in range(0, minread):
                            if bytes1[b] != bytes2[b]:
                                different = True
                                break
                            curpos += 1
                        if minread != bufsize or len(bytes1) != len(bytes2):
                            break
                    if different:
                        print(
                            "{} and {} differ starting from position {} (address: 0x{:X})".format(
                                args.files[i], args.files[j], curpos, curpos
                            )
                        )
                    else:
                        print(
                            "{} and {} have the same content".format(
                                args.files[i], args.files[j]
                            )
                        )
