multidiff
====
Byte compare 2 by 2 a list of files

Description:
------------
`multidiff` is implemented in Python. It takes a list of files in its input and 
compares them 2 by 2. If the `--quiet` flag is provided, a message is printed 
only when files differ. If it is not provided, a message is printed for each pair
of files, specifying the first different byte.

Installation:
-------------
Copy the python script somewhere in the `PATH` or use installation script `install.py`.
See `./install.py --help`.

Usage:
------
See `multidiff.py --help`.
